﻿using MasterSlaveProcessTest.Common;
using MasterSlaveProcessTest.Master;
using MasterSlaveProcessTest.Slave;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterSlaveProcessTest
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            int? numberParam = args.Where(x => Int32.TryParse(x, out _)).Select(x => (int?)Int32.Parse(x)).FirstOrDefault();
            if (numberParam == null)
            {
                Process.Start("MasterSlaveProcessTest.exe", "42");
                Process.Start("MasterSlaveProcessTest.exe", "23");
                Process.Start("MasterSlaveProcessTest.exe", "13");
                return;
            }
            if (args.Contains("/slave"))
            {
                int masterProcessId = Int32.Parse(args.Single(x => x.StartsWith("/MasterProcessId=")).Split('=')[1]);
                Names names = new Names();
                using (new Mutex(true, names.GetMutexName(numberParam), out bool createdNew))
                {
                    if (createdNew)
                    {
                        Application.Run(new SlaveForm(numberParam, masterProcessId));
                    }
                }
            }
            else
            {
                Application.Run(new MasterForm(numberParam));
            }
        }
    }
}
