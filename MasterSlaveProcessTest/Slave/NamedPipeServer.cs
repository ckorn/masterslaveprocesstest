﻿using MasterSlaveProcessTest.Common;
using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterSlaveProcessTest.Slave
{
    internal class NamedPipeServer
    {
        public void Listen(string pipeName, Func<string, string> callback) 
        {
            NamedPipeServerStream pipeServer = new NamedPipeServerStream(pipeName);

            while (true)
            {
                pipeServer.WaitForConnection();

                StreamString streamString = new StreamString(pipeServer);
                string arguments = streamString.ReadString();
                string response = callback(arguments);
                streamString.WriteString(response);
                pipeServer.Flush();
                pipeServer.Disconnect();
            }
        }
    }
}
