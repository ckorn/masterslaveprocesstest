﻿using MasterSlaveProcessTest.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterSlaveProcessTest.Slave
{
    internal class MasterWindowReceiver
    {
        private readonly Names names = new Names();
        private readonly NamedPipeServer namedPipeServer = new NamedPipeServer();
        private bool setInvisibleOnMinimize = false;
        private Form form;

        public void StartReceive(int? numberParam, Form form) 
        {
            this.form = form;
            this.form.Resize += SlaveForm_Resize;
            this.form.TopMost = true;
            Task.Factory.StartNew(() => namedPipeServer.Listen(names.GetPipeNameTopMost(numberParam), topmost)).ContinueWith((t) =>
            {
                if (t.Exception != null)
                {
                    throw t.Exception;
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());
            Task.Factory.StartNew(() => namedPipeServer.Listen(names.GetPipeNameWindowState(numberParam), windowstate)).ContinueWith((t) =>
            {
                if (t.Exception != null)
                {
                    throw t.Exception;
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private string topmost(string arguments)
        {
            if (arguments == "0")
            {
                form.Invoke(new Action(() =>
                {
                    form.TopMost = false;
                }));
            }
            else if (arguments == "1")
            {
                form.Invoke(new Action(() =>
                {
                    form.TopMost = true;
                }));
            }

            return "42";
        }

        private string windowstate(string arguments)
        {
            if (arguments == FormWindowState.Minimized.ToString())
            {
                if (form.WindowState != FormWindowState.Minimized)
                {
                    form.Invoke(new Action(() =>
                    {
                        this.setInvisibleOnMinimize = true;
                        form.WindowState = FormWindowState.Minimized;
                        form.TopMost = false;
                    }));
                }
            }
            else
            {
                form.Invoke(new Action(() =>
                {
                    form.Visible = true;
                    form.WindowState = FormWindowState.Normal;
                    form.TopMost = true;
                }));
            }

            return "42";
        }

        private void SlaveForm_Resize(object sender, EventArgs e)
        {
            if (form.WindowState == FormWindowState.Minimized)
            {
                if (setInvisibleOnMinimize)
                {
                    form.Visible = false;
                    setInvisibleOnMinimize = false;
                }
            }
            else
            {
                form.Visible = true;
            }
        }
    }
}
