﻿using MasterSlaveProcessTest.Common;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterSlaveProcessTest.Slave
{
    public partial class SlaveForm : Form
    {
        private readonly int? numberParam = null;
        private readonly int masterProcessId;
        private readonly Names names = new Names();
        private readonly NamedPipeServer namedPipeServer = new NamedPipeServer();
        private readonly ConfigRepository configRepository = new ConfigRepository();
        private readonly MasterWindowReceiver masterWindowReceiver = new MasterWindowReceiver();

        public SlaveForm(int? numberParam, int masterProcessId)
        {
            this.numberParam = numberParam;
            this.masterProcessId = masterProcessId;
            InitializeComponent();
            this.Text += numberParam.ToString();
        }

        private void SlaveForm_Load(object sender, EventArgs e)
        {
            configRepository.Load($"{nameof(SlaveForm)}_{numberParam}.json", this);
            Task.Factory.StartNew(() => namedPipeServer.Listen(names.GetPipeNameFiles(numberParam), callback)).ContinueWith((t) => MessageBox.Show(GetExceptionText(t.Exception)), TaskContinuationOptions.OnlyOnFaulted);
            masterWindowReceiver.StartReceive(numberParam, this);
            Task.Run(() =>
            {
                try
                {
                    Process masterProcess = Process.GetProcessById(this.masterProcessId);
                    masterProcess.WaitForExit();
                    Environment.Exit(0);
                }
                catch (ArgumentException)
                {
                    Environment.Exit(0);
                }
            });
        }

        private string callback(string arguments) 
        {
            this.Invoke(new Action(() => { this.textBox1.Text = arguments + Environment.NewLine + this.textBox1.Text; }));
            return "42";
        }

        private string GetExceptionText(Exception e) 
        {
            string ret = string.Empty;
            if (e is AggregateException a)
            {
                foreach (Exception exception in a.InnerExceptions)
                {
                    ret += GetExceptionText(exception);
                }
            }
            else
            {
                ret += e.Message + Environment.NewLine;
                ret += e.StackTrace + Environment.NewLine;
                if (e.InnerException != null)
                {
                    ret += GetExceptionText(e.InnerException);
                }
            }
            return ret;
        }

        private void SlaveForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            configRepository.Save($"{nameof(SlaveForm)}_{numberParam}.json", this);
        }
    }
}
