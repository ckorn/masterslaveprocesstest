﻿using MasterSlaveProcessTest.Common;
using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterSlaveProcessTest.Master
{
    internal class NamedPipeClient
    {
        public string Send(string pipeName, string arguments) 
        {
            using (NamedPipeClientStream pipeClient = new NamedPipeClientStream(pipeName)) 
            {
                pipeClient.Connect(1000);

                StreamString streamString = new StreamString(pipeClient);
                streamString.WriteString(arguments);
                pipeClient.Flush();
                string ret = streamString.ReadString();

                pipeClient.Close();

                return ret;
            }                
        }
    }
}
