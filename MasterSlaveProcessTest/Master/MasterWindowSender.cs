﻿using MasterSlaveProcessTest.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterSlaveProcessTest.Master
{
    internal class MasterWindowSender
    {
        private FormWindowState windowState = FormWindowState.Normal;
        private readonly Names names = new Names();
        private readonly NamedPipeClient namedPipeClient = new NamedPipeClient();
        private int? numberParam;
        private Form form;

        public void StartSend(int? numberParam, Form form) 
        {
            this.numberParam = numberParam;
            this.form = form;
            this.form.Activated += MasterForm_Activated;
            this.form.Deactivate += MasterForm_Deactivate;
            this.form.SizeChanged += MasterForm_SizeChanged;
        }

        private void MasterForm_Activated(object sender, EventArgs e)
        {
            using (new Mutex(false, names.GetMutexName(numberParam), out bool createdNew))
            {
                if (!createdNew)
                {
                    try
                    {
                        namedPipeClient.Send(names.GetPipeNameTopMost(numberParam), "1");
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void MasterForm_Deactivate(object sender, EventArgs e)
        {
            using (new Mutex(false, names.GetMutexName(numberParam), out bool createdNew))
            {
                if (!createdNew)
                {
                    try
                    {
                        namedPipeClient.Send(names.GetPipeNameTopMost(numberParam), "0");
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void MasterForm_SizeChanged(object sender, EventArgs e)
        {
            if (form.WindowState != windowState)
            {
                using (new Mutex(false, names.GetMutexName(numberParam), out bool createdNew))
                {
                    if (!createdNew)
                    {
                        try
                        {
                            namedPipeClient.Send(names.GetPipeNameWindowState(numberParam), form.WindowState.ToString());
                        }
                        catch
                        {
                        }
                    }
                }
                windowState = form.WindowState;
            }
        }
    }
}
