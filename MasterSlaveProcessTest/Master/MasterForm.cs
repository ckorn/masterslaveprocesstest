﻿using MasterSlaveProcessTest.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterSlaveProcessTest.Master
{
    public partial class MasterForm : Form
    {
        private readonly int? numberParam = null;
        private readonly ConfigRepository configRepository = new ConfigRepository();
        private readonly Names names = new Names();
        private readonly NamedPipeClient namedPipeClient = new NamedPipeClient();
        private readonly BlockingCollection<List<string>> fileBlockingCollection = new BlockingCollection<List<string>>();
        private readonly MasterWindowSender masterWindowSender = new MasterWindowSender();

        public MasterForm(int? numberParam)
        {
            this.numberParam = numberParam;
            InitializeComponent();
            this.Text += numberParam.ToString();
        }

        private void MasterForm_Load(object sender, EventArgs e)
        {
            Task.Run(() =>
            {
                foreach (List<string> files in fileBlockingCollection.GetConsumingEnumerable())
                {
                    string arguments = JsonConvert.SerializeObject(files, Formatting.Indented);
                    string ret = namedPipeClient.Send(names.GetPipeNameFiles(numberParam), arguments);
                    this.Invoke(new Action(() => textBox1.Text += ret + Environment.NewLine));
                }
            });
            masterWindowSender.StartSend(numberParam, this);
            configRepository.Load($"{nameof(MasterForm)}_{numberParam}.json", this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<string> tempFiles = Directory.EnumerateFiles(Path.GetTempPath()).ToList();
            Random random = new Random(DateTime.Now.Millisecond);
            List<string> randomFiles = new List<string>();
            for (int i = 0; i < 10; i++)
            {
                int index = random.Next(0, tempFiles.Count);
                randomFiles.Add(tempFiles[index]);
            }
            fileBlockingCollection.Add(randomFiles);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (new Mutex(false, names.GetMutexName(numberParam), out bool createdNew))
            {
                if (createdNew)
                {
                    Process.Start(new ProcessStartInfo() { Arguments = $"/MasterProcessId={Process.GetCurrentProcess().Id} /slave {numberParam}", FileName = "MasterSlaveProcessTest.exe" });
                }
            }
        }

        private void MasterForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            configRepository.Save($"{nameof(MasterForm)}_{numberParam}.json", this);
        }
    }
}
