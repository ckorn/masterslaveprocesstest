﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterSlaveProcessTest.Common
{
    internal class ConfigRepository
    {
        public void Load(string name, Form form)
        {
            string fileName = $"{name}.json";
            if (File.Exists(fileName))
            {
                Settings settings = new Settings();
                JsonConvert.PopulateObject(File.ReadAllText(fileName, Encoding.Default), settings);
                form.Location = settings.Location;
                form.Size = settings.Size;
            }
        }

        public void Save(string name, Form form) 
        {
            string fileName = $"{name}.json";
            Settings settings = new Settings()
            {
                Location = form.Location,
                Size = form.Size
            };
            string json = JsonConvert.SerializeObject(settings, Formatting.Indented);
            File.WriteAllText(fileName, json, Encoding.Default);
        }

        private class Settings 
        {
            public Point Location { get; set; }
            public Size Size { get; set; }
        }
    }
}
