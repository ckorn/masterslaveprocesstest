﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterSlaveProcessTest.Common
{
    internal class Names
    {
        private const string PipeNameFiles = "MasterSlaveProcessTest.Files";
        private const string PipeNameKeepAlive = "MasterSlaveProcessTest.KeepAlive";
        private const string PipeNameTopMost = "MasterSlaveProcessTest.TopMost";
        private const string PipeNameWindowState = "MasterSlaveProcessTest.WindowState";
        private const string MutexName = "MasterSlaveProcessTest.Slave";

        public string GetPipeNameFiles(int? number) => $"{PipeNameFiles}{number}";
        public string GetPipeNameKeepAlive(int? number) => $"{PipeNameKeepAlive}{number}";
        public string GetPipeNameTopMost(int? number) => $"{PipeNameTopMost}{number}";
        public string GetPipeNameWindowState(int? number) => $"{PipeNameWindowState}{number}";
        public string GetMutexName(int? number) => $"{MutexName}{number}";
    }
}
